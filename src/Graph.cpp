/*! \file Graph.cpp
*  \brief Implementation of a undirected graph class.
*
*  \author John H. Ring IV
*  \date October, 2017 - April 2018
*/

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <queue>
#include <sstream>
#include <stack>
#include <chrono>

#include "../include/Graph.h"

namespace graphlib {

    Graph::Graph() {
        BaseGraph::m_adjList = std::map<int, std::set<int>>();

        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        BaseGraph::m_gen = std::mt19937_64(seed);
    };


    bool Graph::operator==(Graph &g) {
        return (edges() == g.edges()) && (nodes() == g.nodes());
    }

    void Graph::add_edge(int u, int v) {
        m_adjList[u].insert(v);
        m_adjList[v].insert(u);
    }

    unsigned int Graph::swap_edges(const edge& e1, const edge& e2) {
        unsigned int added = 0;
        if (has_edge(e1) && has_edge(e2)) {
            if (m_gen() % 2 && !(degree(e1.first)==1 && degree(e2.second)==1) && !(degree(e2.first)==1 && degree(e1.second)==1)) {
                if((e1.first != e2.second) && (e2.first!= e1.second) && !has_edge(e1.first, e2.second) && !has_edge(e2.first, e1.second)) {
                    add_edge(e1.first, e2.second);
                    add_edge(e2.first, e1.second);
                    added = 1;
                }

            } else {
                if((e1.first != e2.first) && (e1.second != e2.second) && !has_edge(e1.first, e2.first) && !has_edge(e1.second, e2.second)) {
                    add_edge(e1.first, e2.first);
                    add_edge(e1.second, e2.second);
                    added = 1;
                }
            }
            if (added) {
                remove_edge(e1.first, e1.second);
                remove_edge(e2.first, e2.second);
            }
        }
        return added;
    }

    void Graph::remove_edge(int u, int v) {
        if (has_edge(u, v)) {
            m_adjList[u].erase(v);
            m_adjList[v].erase(u);
        }
    }

    std::vector<std::pair<int, int>> Graph::edges() {
        std::vector<std::pair<int, int>> edges;
        for (auto&[key, val] : m_adjList) {
            for (auto elm : val) {
                if (key <= elm)
                    edges.emplace_back(std::make_pair(key, elm));
            }
        }
        return edges;
    }

    unsigned int Graph::number_of_edges() {
        auto num_self = number_of_selfloops();
        unsigned long num = 0;
        for (auto &it: m_adjList) {
            num += it.second.size();
        }
        return static_cast<unsigned int>(((num - num_self) / 2) + num_self);
    }

    bool Graph::is_directed() {
        return false;
    }

    // todo add weight parameter
    unsigned int Graph::degree(int id) {
        return (unsigned int) m_adjList[id].size();
    }

    // todo has_node check
    std::map<int, unsigned int> Graph::degree(std::vector<int> ids /*= {}*/) {
        std::map<int, unsigned int> degrees = {};
        if (ids.empty()) {
            ids = nodes();
        }
        for (auto id : ids) {
            degrees[id] = (unsigned int) m_adjList[id].size();
        }
        return degrees;
    }

    std::vector<int> Graph::edge_list(int id) {
        return neighbors(id);
    }

}