#pragma clang diagnostic push
#pragma ide diagnostic ignored "openmp-use-default-none"
#include <iostream>
#include <omp.h>
#include <cmath>
#include <string>
#include "../include/Graph.h"
#include "../include/Models.h"
#include "../include/connected.h"

using namespace std;
// using namespace graphlib;

using graphlib::Graph, graphlib::empty_graph, graphlib::is_connected, graphlib::components_nodes;
typedef pair<unsigned int, unsigned int> edge;

/*!
 * Finds the minimum value and it's associated key in the map
 *
 * @tparam KeyType
 * @tparam ValueType
 * @param x Map to search
 * @return pair of key and value
 */
template<typename KeyType, typename ValueType>
std::pair<KeyType,ValueType> get_min( const std::map<KeyType,ValueType>& x ) {
    using pairtype=std::pair<KeyType,ValueType>;
    return *std::min_element(x.begin(), x.end(), [] (const pairtype & p1, const pairtype & p2) {
        return p1.second < p2.second;
    });
}


/*
 * Clone of: https://networkx.github.io/documentation/networkx-1.10/_modules/networkx/algorithms/graphical.html
 */
bool is_valid_degree_sequence_havel_hakimi(vector<unsigned int>& deg_seq) {
    auto p = static_cast<const unsigned int>(deg_seq.size());

    vector<unsigned int> num_degs(p, 0U);
    unsigned int dmax = 0, dmin = p, dsum = 0, n = 0;

    for (const auto& deg : deg_seq) {
        if (deg >= p)
            return false;
        else if (deg > 0) {
            dmax = max(dmax, deg);
            dmin = min(dmin, deg);
            dsum += deg;
            ++n;
            num_degs[deg] += 1;
        }
    }
    if (dsum % 2 || dsum > n * (n - 1))
        return false;

    if (n == 0 || 4 * dmin * n >= pow(dmax + dmin + 1, 2))
        return true;

    vector<unsigned int> modstubs(dmax+1, 0U);

    while (n > 0) {
        while (num_degs[dmax] == 0)
            --dmax;
        if (dmax > n - 1)
            return false;

        --num_degs[dmax];
        --n;
        unsigned int mslen = 0;
        unsigned int k = dmax;
        for (unsigned int i = 0; i < dmax; ++i) {
            while (num_degs[k] == 0)
                --k;

            --num_degs[k];
            --n;

            if (k > 1) {
                modstubs[mslen] = k-1;
                ++mslen;
            }
        }
        for (unsigned int i = 0; i < mslen; ++i) {
            auto stub = modstubs[i];
            ++num_degs[stub];
            ++n;
        }
    }

    return true;
}

// clone of https://networkx.github.io/documentation/networkx-1.10/_modules/networkx/generators/degree_seq.html
int havel_hakimi_graph(vector<unsigned int> deg_seq, Graph& graph) {
    auto p = static_cast<const unsigned int>(deg_seq.size());
    empty_graph(p, graph);

    if (!is_valid_degree_sequence_havel_hakimi(deg_seq)) {
        cout << "Invalid degree sequence\n";
        return -1;
    }
    if (graph.is_directed()) {
        cout << "Directed graphs are not supported\n";
        return -1;
    }

    vector<vector<unsigned int>> num_degs(p, vector<unsigned int>());

    unsigned int dmax = 0, dsum = 0, n = 0;
    for (const auto& deg : deg_seq) {
        if (deg > 0) {
            num_degs[deg].emplace_back(n);
            dmax = max(dmax, deg);
            dsum += deg;
            ++n;
        }
    }

    if (n == 0)
        return 0;

    vector<edge> modstubs(dmax+1, {edge(0U, 0U)});

    while (n > 0) {

        while (num_degs[dmax].empty())
            --dmax;

        if (dmax > n - 1) {
            cout << "Non-graphical integer sequence";
            return -1;
        }

        auto source = num_degs[dmax].back();
        num_degs[dmax].pop_back();
        --n;
        auto mslen = 0U;
        auto k = dmax;
        for (auto i = 0U; i < dmax; ++i) {
            while (num_degs[k].empty())
                k -= 1;
            auto target = num_degs[k].back();
            num_degs[k].pop_back();
            graph.add_edge(source, target);
            --n;
            if (k > 1) {
                modstubs[mslen] = edge(k - 1, target);
                ++mslen;
            }
        }

        for (auto i = 0U; i < mslen; ++i) {
            auto [stubval, stubtarget] = modstubs[i];
            num_degs[stubval].emplace_back(stubtarget);
            ++n;
        }
    }
    return 0;
}

// https://networkx.github.io/documentation/networkx-1.10/_modules/networkx/algorithms/tree/recognition.html
bool is_tree(Graph g) {
    if (g.number_of_nodes() == 0) {
        cout << "Warning: graph has no nodes\n";
        return false;
    }

    if (g.number_of_edges() != g.number_of_nodes() - 1)
        return false;

    // todo use is weakly connected for directed graphs
    return is_connected(g);
}

bool make_connected(Graph& g,  std::mt19937_64 *gen = nullptr) {
    if (gen == nullptr) {
        auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        gen = new std::mt19937_64(seed);
    }
    //auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    //auto gen = std::mt19937_64(seed);
    const unsigned int n = g.number_of_nodes();

    if (g.number_of_edges() < n -1) {
        cout << "\ngraph::make_connected() failed : #edges < #vertices-1\n";
        return false;
    }

    if (get_min(g.degree()).second == 0) {
        cout << "Error: vertex with degree 0\n";
        return false;
    }

    auto comps = components_nodes(g);

    Graph cx;
    Graph cy = Graph();
    while (comps.size() > 1) {
        //cout << "Comps: " << comps.size() << endl;
        // select first non-tree component as cx
        int cx_idx = -1;
        int cy_idx = -1;
        for (auto& comp : comps) {
            ++cx_idx; //index of comp in comps
            cx = g;
            for (const auto& vert : g.nodes() ) { //remove nodes that are not in component
                if (!(find(comp.begin(), comp.end(), vert) != comp.end()))
                    cx.remove_node(vert);
            }
            if (is_tree(cx)) {
                if (cy_idx == -1) {// cy is not initialized
                    cy = cx;
                    cy_idx = cx_idx;
                }
                continue;
            }

            // cx has been selected initialize cy if required
            if (cy_idx == -1) {
                //vector<int> cyComp;
                if (cx_idx > 0)
                    cy_idx = cx_idx - 1;
                else
                    cy_idx = cx_idx + 1;
                cy = g;
                for (const auto& vert : g.nodes() ) { //remove nodes that are not in component
                    if (!(find(comps[cy_idx].begin(), comps[cy_idx].end(), vert) != comps[cy_idx].end()))
                        cy.remove_node(vert);
                }
            }
            break;
        }
        // refactor: make subgraph function
        // rewire
        // recalculate components

        // select a safe edge from cx
        auto edges = cx.edges();
        shuffle(edges.begin(), edges.end(), (*gen));
        pair<int, int> cx_edge;
        for (auto e : edges) {
            cx.remove_edge(e.first, e.second);
            if (components_nodes(cx).size() > 1) {
                cx.add_edge(e.first, e.second);
                continue;
            }
            cx_edge = e;
            break;
        }
        // select any edge from cy
        auto cy_edge = cy.edges()[(*gen)() % cy.number_of_edges()];

        // swap cx_edge and cy_edge
        g.swap_edges(cx_edge, cy_edge);

        // recalculate components (merge comps[cx_idx] and comps[cy_idx])
        comps[cx_idx].insert(comps[cx_idx].begin(), comps[cy_idx].begin(), comps[cy_idx].end());
        comps.erase(comps.begin() + cy_idx);
    }
    return true;
}


int random_edge_swap(Graph& g, std::mt19937_64 *gen) {
   auto edges = g.edges();
   auto m = edges.size();

   edge e1 = edges[(*gen)() % m];
   edge e2 = edges[(*gen)() % m];
   return g.swap_edges(e1, e2);
}

// A replication attempt of graph_molloy_hash::shuffle
void shuffle_edges(Graph& g, unsigned int times) {
    unsigned int nb_swaps = 0;
    //unsigned int window = min(g.number_of_edges() * 2, times) / 10;
    unsigned int window = times * 2;

    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);

    Graph backup = g;

    while(times > nb_swaps) {
        //cout << "Swaps: " << nb_swaps << " " << times << endl;

        unsigned int swaps = 0;

        for (auto i = 0U; i < window; ++i) {
            swaps += random_edge_swap(g, &gen);
        }

        // test connectivity
        if (is_connected(g)) {
            nb_swaps += swaps;
            backup = g;
        }
        else
            g = backup;
    }
}

void fast_shuffle_edges(Graph& g, unsigned int times) {
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);

    unsigned int swaps = 0;
    while (swaps < times) {
        swaps += random_edge_swap(g, &gen);
    }

    if (!is_connected(g)) {
        connect_components(g, &gen);
    }
}

void tree_time(int ary) {
    std::ofstream file;
    file.open("../out/tree_time_" + to_string(ary) + "ary.csv");
    file << "Nodes,Edges,Edge_Frac,Time\n";
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937_64 gen(seed);
    Graph g;
    std::vector<int> ns = {3200};//{25, 50, 100, 200, 400, 800, 1600, 3200};
    std::vector<double> es = {};
    double e_frac_max = 25;
    while(e_frac_max > 0) {
        es.emplace_back(e_frac_max);
        e_frac_max -= 2.5;
    }
    auto times = 4500;

    for (auto n :ns) {
        for (auto e : es) {
            graphlib::full_rary_tree(ary, n, g);
            auto nodes = g.nodes();
            auto num_nodes = nodes.size();
            auto total_edges = g.number_of_edges() + floor((num_nodes / e) + 0.5);

            auto g_bak = g;
            double trials = 30;
            double runtime = 0.0;
            for (auto i = 0; i < trials; i++) {
                g = g_bak;
                while (g.number_of_edges() != total_edges) {
                    int source = nodes[(gen)() % num_nodes];
                    int target = nodes[(gen)() % num_nodes];
                    g.add_edge(source, target);
                }
                auto start = std::chrono::high_resolution_clock::now();
                fast_shuffle_edges(g, times);
                auto finish = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> elapsed = finish - start;
                runtime += elapsed.count();
                file << n << ", " << total_edges << ", " << e << ", " << elapsed.count() << endl;
            }
            std::cout << "N: " << g.number_of_nodes() << " e: " << g.number_of_edges()  << " Time: " << runtime / trials << " s\n";
        }
    }
    file.close();
}

void test_sampling() {
    vector<unsigned int> deg_seq = {1, 1, 1, 2, 2, 2, 3};
    int iters = 1000000;
    auto counts = std::vector<unsigned int>(3, 0);

    Graph base;
    havel_hakimi_graph(deg_seq, base);

    auto times = 10 * base.number_of_edges();

    auto start = std::chrono::high_resolution_clock::now();
    #pragma omp parallel for
    for(int i = 0; i < iters; ++i) {
        Graph g = base;
        //shuffle_edges(g, times);
        //connected_simple_configuration_model(deg_seq, g, &gen);
        fast_shuffle_edges(g, times);

        // Node 6 always has degree 3
        auto neigh = g.neighbors(6);
        int model = 0;
        for (auto n : neigh) {
            model += g.degree(n);
        }
        model %= 4;
        #pragma omp critical
        counts[model]++;
    }
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;

    std::cout << "Duration: " << elapsed.count() << std::endl;

    std::vector<double> expected = {.3, .6, .1};
    for (int i = 0; i < counts.size(); ++i) {
        double prob = counts[i] / (iters * 1.0);
        std::cout << i << ": " << prob << " (" << expected[i] << ")" << std::endl;
    }
}


int main() {
    //omp_set_num_threads(omp_get_max_threads());

    //test_sampling();
    tree_time(3);
}