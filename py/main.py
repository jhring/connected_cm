import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.ticker import FuncFormatter, MaxNLocator
import numpy as np


def time_plots(ary):
    df = pd.read_csv(f"../out/tree_time_{ary}ary.csv")
    df["Nodes"].astype(int)

    for k, block in df.groupby("Nodes"):
        fig, ax = plt.subplots()
        fig.set_size_inches(10, 10)
        tmp = block[["Time", "Edges"]].groupby("Edges").describe()
        tmp.columns = tmp.columns.droplevel(0)
        tmp[["min", "25%", "50%", "75%", "max"]].plot(ax=ax)
        plt.title(f"{ary}Ary with {k} Nodes")

        labels = list(tmp.index)
        plt.xticks(labels)
        plt.xticks(rotation=70)

        plt.ylabel("Time (s)")
        plt.savefig(f"../out/{ary}ary_{k}_nodes.png", dpi=80)
        plt.close(fig)


def heatmap_log(ary):
    df = pd.read_csv(f"../out/tree_time_{ary}ary.csv")
    num = len(df["Edge_Frac"].unique())
    df.drop("Edges", inplace=True, axis=1)
    df = df.groupby(["Nodes", "Edge_Frac"]).median()
    vals = df.values.reshape((-1, num))
    plt.imshow(vals, origin="lower", norm=LogNorm(vmin=vals.min(), vmax=vals.max()))
    ax = plt.gca()
    ax.set_xticklabels([None] + list(df.index.levels[1]))
    ax.set_yticklabels([None] + list(df.index.levels[0]))
    plt.xlabel(r"$\frac{N}{val}$ edges added")
    plt.ylabel("Nodes")
    plt.title(f"Median {ary}Ary Logged Runtime Scaling")
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Log Runtime (s)', rotation=270, labelpad=15)
    plt.savefig(f"../out/{ary}ary_log_heatmap.png", dpi=80)
    plt.close()


def heatmap(ary):
    df = pd.read_csv(f"../out/tree_time_{ary}ary.csv")
    num = len(df["Edge_Frac"].unique())
    df.drop("Edges", inplace=True, axis=1)
    df = df.groupby(["Nodes", "Edge_Frac"]).median()
    vals = df.values.reshape((-1, num))
    plt.imshow(vals, origin="lower")
    ax = plt.gca()
    ax.set_xticklabels([None] + list(df.index.levels[1]))
    ax.set_yticklabels([None] + list(df.index.levels[0]))
    plt.xlabel(r"$\frac{N}{val}$ edges added")
    plt.ylabel("Nodes")
    plt.title(f"Median {ary}Ary Runtime Scaling")
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Runtime (s)', rotation=270, labelpad=15)
    plt.savefig(f"../out/{ary}ary_heatmap.png", dpi=80)
    plt.close()


if __name__ == '__main__':
    for i in range(1, 6):
        time_plots(i)
        heatmap_log(i)
        heatmap(i)
