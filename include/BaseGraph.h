/*! \file BaseGraph.h
 *  \brief Backbone of this libraries graph implementations. This class is intended for internal use only.
 *
 *  \author John H. Ring IV
 *  \date April 2018
 *
 *  The primary purpose of this file is to allow core data structures such as edge lists to be templated.
 */

#ifndef GRAPHLIB_BASEGRAPH_H
#define GRAPHLIB_BASEGRAPH_H


#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <queue>
#include <sstream>
#include <stack>
#include <set>
#include <map>
#include <random>


namespace graphlib {

    /*! \typedef std::pair<int, int> edge
     *  \brief An edge between two vertices
     *
     *  In a directed graph the edge is from the vertex specified by the first
     *  integer to the second.
    */
    typedef std::pair<int, int> edge;

    /*!
     * @brief Backbone for this project's graph classes (Graph, MultiGraph ...)
     *
     * This class implements the core functionality shared by all graph types. Many of these functions will be overridden
     * in the various derived classes. This file is intended for internal use only.
     *
     * @tparam T Data structure for the edge list
     */
    template<class T>
    class BaseGraph {
    public:

        /*! @brief Checks if a node is in the graph.
         *
         * @param id of node to check
         * @return True if specified node is in the graph
         */
        bool has_node(int id) {
            return m_adjList.count(id) == 1;
        }

        /*!
         * @brief Adds a node to the graph
         *
         * If a node with the specified ID already exists no action is taken.
         *
         * @param id
         */
        void add_node(int id) {
            if (!has_node(id)) {
                m_adjList.emplace(id, T());
            }
        }

        /*!
         * @brief Adds multiple nodes to the graph.
         *
         * See BaseGraph::add_node
         *
         * @param nodes The nodes to add
         */
        void add_nodes(const std::vector<int> &nodes) {
            for (auto const &id: nodes) {
                add_node(id);
            }
        }

        /*!
         * @brief Removes all edges and nodes from the graph
         */
        void clear() {
            m_adjList.clear();
        }

        /*!
         * @brief Gets the neighbors of a specified node.
         *
         * Node n1 has a neighbor n2 if there is an edge from n1 to n2.
         * An empty vector is returned if the specified node does not exist.
         *
         * @param id node to check
         * @return neighbors of id
         */
        virtual std::vector<int> neighbors(int id) {
            if (has_node(id))
                return std::vector<int>(m_adjList[id].begin(), m_adjList[id].end());
            else
                return std::vector<int>();
        }

        /*!
         * @brief Removes a node from the graph.
         *
         * All edges containing the specified node are also removed.
         * If the node does not exist no action is taken.
         *
         * @param id node to remove
         */
        void remove_node(int id) {
            auto const &neigh = neighbors(id);
            for (auto const &node : neigh) {
                m_adjList[node].erase(id);
            }
            m_adjList.erase(id);
        }

        /*!
         * @brief Removes a list of nodes from the graph.
         *
         * See BaseGraph::remove_node
         *
         * @param nodes to remove
         */
        void remove_nodes(const std::vector<int> &nodes) {
            for (auto node : nodes) {
                remove_node(node);
            }
        }

        /*!
         * @brief Gets the number of nodes in the graph.
         *
         * @return Number of nodes in the graph.
         */
        unsigned int number_of_nodes() {
            return (unsigned int) m_adjList.size();
        }

        /*!
         * @brief Gets the number of edges in the graph.
         *
         * Pure virtual function as each graph type has unique requirements.
         *
         * @return Number of edges in the graph.
         */
        virtual unsigned int number_of_edges() = 0;

        /*!
         * @brief Gets a vector of all nodes
         *
         * @return All node IDs.
         */
        std::vector<int> nodes() {
            std::vector<int> nodes;
            nodes.reserve(number_of_nodes());
            for (auto &it : m_adjList) {
                nodes.push_back(it.first);
            }
            return nodes;
        }

        /*!
         * @brief Create an edge between u and v.
         *
         * Pure virtual function as each graph type has unique requirements.
         *
         * @param u
         * @param v
         */
        virtual void add_edge(int u, int v) = 0;

        virtual unsigned int swap_edges(const edge& e1, const edge& e2) = 0;

        /*!
         * @brief Add edges to the graph.
         *
         * See BaseGraph::add_edge
         *
         * @param edges to add
         */
        void add_edges(const std::vector<edge> &edges) {
            for (auto &edge: edges) {
                add_edge(edge.first, edge.second);
            }
        }

        /*!
         * @brief Checks if there exists an edge between u and v
         *
         * False if either node does not exist.
         *
         * @param u node ID
         * @param v node ID
         * @return True if there is an edge between u and v
         */
        bool has_edge(int u, int v) {
            return m_adjList[u].count(v) > 0;
        }

        bool has_edge(edge e) {
            return m_adjList[e.first].count(e.second) > 0;
        }


        /*!
         * @brief Removes an edge between nodes u and v.
         *
         * No action is taken if the edge does not exist.
         *
         * @param u
         * @param v
         */
        virtual void remove_edge(int u, int v) = 0;

        /*!
         * @brief Removes edges from the graph.
         *
         * See BaseGraph::remove_edge
         *
         * @param edges to remove
         */
        void remove_edges(std::vector<std::pair<int, int>> &edges) {
            for (auto &edge: edges) {
                remove_edge(edge.first, edge.second);
            }
        }

        /*!
         * @brief Gets a vector of all edges in the graph
         * @return edges
         */
        virtual std::vector<std::pair<int, int>> edges() = 0;

        /*!
         * Getter for the adjacency list.
         * @return m_adjList
         */
        T adjacency_list() {
            return m_adjList;
        }

        /*!
         * @brief Finds all nodes that have an edge to themselves.
         *
         * @return IDs of nodes that have self loops
         */
        std::vector<int> nodes_with_selfloops() {
            std::vector<int> ret;
            for (auto n: nodes()) {
                if (m_adjList.count(n) > 0) {
                    ret.emplace_back(n);
                }
            }
            return ret;
        }

        /*!
         * Gets all edges where the edge source and destination are the same.
         * @return Self-looping edges
         * @todo Move this outside of the class
         */
        std::vector<std::pair<int, int>> selfloop_edges() {
            std::vector<std::pair<int, int>> ret;
            for (auto n: nodes()) {
                auto occurrences = m_adjList.count(n);
                while (occurrences) {
                    ret.emplace_back(std::make_pair(n, n));
                    --occurrences;
                }
            }
            return ret;
        }

        /*!
         * @brief Gets the number of selfloop edges
         *
         * See BaseGraph::selfloop_edges
         *
         * @return number of selfloops
         */
        unsigned int number_of_selfloops() {
            unsigned int count = 0;
            for (auto n: nodes()) {
                count += m_adjList[n].count(n);
            }
            return count;
        }

        /*!
         * @brief Checks if the graph type is directed
         *
         * @return True if a directed graph
         */
        virtual bool is_directed() = 0;

        /*!
         * @brief Gets the degree of a node
         *
         * @param id
         * @return node degree
         */
        virtual unsigned int degree(int id) = 0;

        /*!
         * @brief Gets the degree os each specified node
         * @param ids
         * @return the degree of each node in ids
         */
        virtual std::map<int, unsigned int> degree(std::vector<int> ids = {}) = 0;

        virtual std::vector<int> edge_list(int id) = 0;


    protected:
        /*!
         * Adjacency List representation of the graph.
         */
        std::map<int, T> m_adjList;

        /*!
         * Random number generator.
         */
        std::mt19937_64 m_gen;
    };

} /* namespace graphlib */

#endif //GRAPHLIB_BASEGRAPH_H

