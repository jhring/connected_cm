/*! \file Graph.h
 *  \brief Definition of a undirected graph class.
 *
 *  \author John H. Ring IV
 *  \date October, 2017 - April 2018
 */

#ifndef GRAPHLIB_LIBRARY_H
#define GRAPHLIB_LIBRARY_H

#include <set>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <memory>
#include "BaseGraph.h"


namespace graphlib {

    /*!
     * @brief A undirected graph with self-loops.
     *
     * Implemented with adjacency list. Vertices are integers.
     * Edges are unweighted and at most a single edge is allowed between two nodes.
     */
    class Graph : public BaseGraph<std::set<int>>{
    public:

        /*!
         * @brief Default constructor for Graph.
         *
         * Creates an empty instance of Graph; no nodes, no edges.
         */
        Graph();

        /*!
         * @brief Adds an edge from u to v.
         *
         * If nodes in the edge do not exist they are added to the graph.
         * Parallel edges are ignored.
         *
         * @param u
         * @param v
         */
        void add_edge(int u, int v) override;

        unsigned int swap_edges(const edge& e1, const edge& e2) override;

        /*!
         * @brief Removes an edge between nodes u and v.
         *
         * No action is taken if the edge does not exist.
         *
         * @param u
         * @param v
         */
        void remove_edge(int u, int v) override;

        /*!
         * @brief Gets a vector of all edges in the graph
         * @return edges
         */
        std::vector<std::pair<int, int>> edges() override;

        /*!
         * @brief Gets the number of edges in the graph.
         *
         * Each undirected edge is only counted once. Ie. (u, v) and (v, u) would only be one edge.
         *
         * @return Number of edges in the graph.
         */
        unsigned int number_of_edges() override;

        //unsigned int in_degree(int id);

        //std::vector<unsigned int> in_degree(std::vector<int> ids = {});

        // DiGraph to_directed()

        //Graph subgraph(std::vector<int>);

        // add star
        // add path
        // add cycle

        /*!
         * @brief False as the graph is undirected
         *
         * Used to check what type of graph an instance is.
         *
         * @return False
         */
        bool is_directed();

        /*!
         * @brief Gets the degree of a node
         *
         * @param id
         * @return node degree
         */
        unsigned int degree(int id);

        /*!
         * @brief Gets the degree of each specified node or all nodes if none are specified
         * @param ids
         * @return the degree of each node in ids
         */
        std::map<int, unsigned int> degree(std::vector<int> ids={});

        std::vector<int> edge_list(int id);

        /*!
         * Equivalence check for graphs
         *
         * True is graphs have the same nodes and edges.
         *
         * @param g
         * @return equality
         */
        bool operator==(Graph &g);
    };

} /* namespace graphlib */

#endif