/*! \file connected.h
 *  \brief Contains connectedness measures.
 *
 *  \author John H. Ring IV
 *  \date April 2018 - May 2018
 */

#ifndef GRAPHLIB_CONNECTED_H
#define GRAPHLIB_CONNECTED_H

#include "BaseGraph.h"

namespace graphlib {

    /*!
     * Finds the shortest path length from the source node to all other reachable nodes in the graph.
     *
     * This function is useful for finding connected components and solving other similar problems.
     *
     * @tparam T edgelist datatype
     * @param g Graph to examine
     * @param source node
     * @param cutoff (optional) max distance from source to search
     * @return the shortest path lengths
     */
    template<typename T>
    std::map<int, unsigned int> single_source_shortest_path_length(BaseGraph <T> &g, int source, int cutoff = -1) {
        std::map<int, unsigned int> seen = {};
        std::map<int, bool> visited;
        for (auto id : g.nodes()) {
            visited[id] = false;
        }
        unsigned int level = 0;
        std::vector<int> nextlevel = {source}; // nodes to check at the next level
        std::vector<int> thislevel, neigh;
        while (!nextlevel.empty()) {
            thislevel = nextlevel;
            nextlevel = {};
            for (auto v : thislevel) {
                if (!visited[v]) {
                    seen[v] = level;
                    visited[v] = true;
                    neigh = g.neighbors(v);
                    nextlevel.insert(nextlevel.begin(), neigh.begin(), neigh.end());
                }
            }
            if (cutoff >= 0 && cutoff <= level)
                break;
            level = level + 1;
        }
        return seen;
    }

    /*!
     * Finds the connected components in a graph.
     *
     * @tparam T edgelist datatype
     * @param g graph to examine
     * @return The nodes in the components in the graph
     */
    template<typename T>
    std::vector<std::vector<int>> components_nodes(BaseGraph <T> &g) {
        auto nodes = g.nodes();
        std::vector<std::vector<int>> components = {};
        int idx = 0;
        while (!nodes.empty()) {
            components.emplace_back(std::vector<int>());
            int start = nodes[0];
            for (auto &item : single_source_shortest_path_length(g, start)) {
                components[idx].emplace_back(item.first);
                nodes.erase(std::remove(nodes.begin(), nodes.end(), item.first), nodes.end());
            }
            ++idx;
        }
        return components;
    }

    template<typename T>
    void components_nodes(BaseGraph<T> &g, std::vector<std::vector<int>>& components, std::map<int, unsigned int>& node_map) {
        components.clear();
        node_map.clear();
        auto nodes = g.nodes();
        int idx = 0;
        while (!nodes.empty()) {
            components.emplace_back(std::vector<int>());
            int start = nodes[0];
            for (auto &item : single_source_shortest_path_length(g, start)) {
                components[idx].emplace_back(item.first);
                node_map[item.first] = idx;
                nodes.erase(std::remove(nodes.begin(), nodes.end(), item.first), nodes.end());
            }
            ++idx;
        }
    }

    template<typename T>
    void update_components_nodes(BaseGraph<T> &g, std::vector<std::vector<int>>& components, std::map<int, unsigned int>& node_map, const std::vector<int>& changed, std::vector<int> dirty_comps) {
        //order affected components
        std::vector<int> nodes = {};
        int removed = 0;
        if(dirty_comps[0]<dirty_comps[1]) {
            int tmp = dirty_comps[0];
            dirty_comps[0] = dirty_comps[1];
            dirty_comps[1] = tmp;
        }
        // remove affected components and get nodes
        for (int c0=0; c0<2; ++c0) {
            int idx = dirty_comps[c0];
            auto comp_nodes = components[idx];
            removed += components[idx].size();
            nodes.insert(nodes.end(), comp_nodes.begin(), comp_nodes.end());
            components.erase(components.begin() + idx);
        }
        // recalulate missing
        int added = 0;
        int idx = components.size();
        while (!nodes.empty()) {
            components.emplace_back(std::vector<int>());
            int start = nodes[0];
            for (auto &item : single_source_shortest_path_length(g, start)) {
                components[idx].emplace_back(item.first);
                nodes.erase(std::remove(nodes.begin(), nodes.end(), item.first), nodes.end());
            }
            added += components[idx].size();
            ++idx;
        }
    }

    /*!
    * Finds the connected components in a graph.
    *
    * @tparam T edgelist datatype
    * @param g graph to examine
    * @return The edges in components in the graph
    */
    template<typename T>
    std::vector<std::vector<edge>> components_edges(BaseGraph <T> &g) {
        auto nodes = g.nodes();
        std::vector<std::vector<edge>> components_edges = {};
        int idx = 0;
        while (!nodes.empty()) {
            components_edges.emplace_back(std::vector<edge>());
            int start = nodes[0];
            for (auto &item : single_source_shortest_path_length(g, start)) {
                std::vector<int> neighs = g.neighbors(item.first);
                for (auto &neigh : neighs) {
                    components_edges[idx].emplace_back(item.first, neigh);
                }
                nodes.erase(std::remove(nodes.begin(), nodes.end(), item.first), nodes.end());
            }
            ++idx;
        }
        return components_edges;
    }

    /*!
     * True if the graph is connected
     *
     * @todo Modify for directed graphs
     *
     * @tparam T edgelist datatype
     * @param G Graph to examine
     * @return True if G is connected
     */
    template<typename T>
    bool is_connected(BaseGraph <T> &G) {
        auto nodes = G.nodes();
        auto componet_size = single_source_shortest_path_length(G, nodes[0]).size();
        return componet_size == nodes.size();
    }

} /* namespace graphlib*/

#endif //GRAPHLIB_CONNECTED_H
