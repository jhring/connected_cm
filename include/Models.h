/*! \file Models.h
 *  \brief Generation of standard graph models
 *
 *  \author John H. Ring IV
 *  \date April 2018
 *
 *  @todo check for zeros in connected deg baised models
 */

#include <random>
#include <iostream>
#include <chrono>
#include <algorithm>
#include <stack>
#include <queue>

#include "BaseGraph.h"
#include "connected.h"

namespace graphlib {

    /*!
     * Creates a graph with n nodes and no edges
     * @tparam T edgelist data type
     * @param n number of nodes to use
     * @param[out] g graph to add nodes to
     */
    template<typename T>
    void empty_graph(unsigned int n, BaseGraph<T> &g) {
        g.clear();
        for (unsigned int i = 0; i < n; ++i) {
            g.add_node(i);
        }
    }

    /*!
     * Creates a fully connected graph with n nodes
     * @tparam T edgelist data type
     * @param n number of nodes to use
     * @param[out] g graph to modify
     */
    template<typename T>
    void complete_graph(unsigned int n, BaseGraph<T> &g) {
        empty_graph(n, g);

        std::vector<edge> edges;
        if (n > 1) {
            if (g.is_directed()) {
                for (unsigned int i = 0; i < n; ++i) {
                    for (unsigned int j = 0; j < n; ++i) {
                        if (i != j)
                            edges.emplace_back(std::make_pair(i, j));
                    }
                }
            } else {
                for (unsigned int i = 0; i < n; ++i) {
                    for (unsigned int j = i + 1; j < n; ++j) {
                        edges.emplace_back(std::make_pair(i, j));
                    }
                }
            }
        }
        g.add_edges(edges);
    }

    /*!
    * Creates a full r-ary tree of n vertices.
    *
    * @tparam T edgelist data type
    * @param r branching factor of the tree
    * @param n Number of nodes in the tree
    * @param[out] g graph to modify
    */
    template<typename T>
    void full_rary_tree(unsigned int r, unsigned int n, BaseGraph<T> &g) {
        empty_graph(n, g);
        if (n == 0)
            return;
        auto nodes = std::stack<int>();
        for (int i = n-1; i >= 0; i--) {
            nodes.emplace(i);
        }
        std::queue<int> parents;
        parents.emplace(nodes.top());
        nodes.pop();

        while (!parents.empty()) {
            auto source = parents.front();
            parents.pop();
            for (int i = 0; i < r; i++) {
                if (!nodes.empty()) {
                    auto target = nodes.top();
                    nodes.pop();
                    parents.emplace(target);
                    g.add_edge(source, target);
                } else
                    return;
            }
        }
    }

    /*!
     * The typical Erdos-Renyi model
     * @tparam T edgelist datatype
     * @param n number of nodes
     * @param M number of edges
     * @param[out] g graph to modify
     */
    template<typename T>
    void er_model(unsigned int n, unsigned int M, BaseGraph<T> &g, std::mt19937_64 *gen = nullptr) {
        if (M > (n * (n - 1) / 2)) {
            std::cerr << "Error: er_model, M = " << M << " is too large for " << n << "\n";
            std::_Exit(1);
        }

        if (gen == nullptr) {
            auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            gen = new std::mt19937_64(seed);
        }

        for (unsigned int i = 0; i < n; ++i) {
            g.add_node(i);
        }
        if (M < (n * (n - 1) / 4)) {
            for (unsigned int i = 0; i < M; ++i) {
                auto e1 = (int) ((*gen)() % n);
                auto e2 = (int) ((*gen)() % n);
                if ((e1 == e2) || g.has_edge(e1, e2))
                    --i;
                else
                    g.add_edge(e1, e2);
            }
        } else {
            complete_graph(n, g);
            auto edges = g.edges();

            while (edges.size() != M) {
                auto idx = (int) ((*gen)() % edges.size());
                g.remove_edge(edges[idx].first, edges[idx].second);
                edges.erase(edges.begin() + idx);
            }
        }
    }

    /*!
     * Creates a configuration mode with exactly the specified degree sequence without allowing self-loops
     * and parallel edges
     * @tparam T edgelist data type
     * @param deg_sequence to use
     * @param[out] g graph to modify
     */
    template<typename T>
    void simple_configuration_model(
            std::vector<unsigned int> &deg_sequence, BaseGraph<T> &g, std::mt19937_64 *gen = nullptr) {

        int M = std::accumulate(deg_sequence.begin(), deg_sequence.end(), 0);
        int N = deg_sequence.size();

        if (M % 2 != 0 || M > (N * (N - 1) / 2)) {
            std::cerr << "Error: simple_configuration_model, Invalid degree sequence.\n";
            std::_Exit(1);
        }

        if (gen == nullptr) {
            auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            gen = new std::mt19937_64(seed);
        }

        empty_graph(N, g);

        std::vector<int> stublist;
        stublist.reserve(M);

        for (int i = 0; i < N; ++i) {
            for (unsigned int j = 0; j < deg_sequence[i]; ++j) {
                stublist.push_back(i);
            }
        }

        std::vector<int> problems;

        std::shuffle(stublist.begin(), stublist.end(), *gen);
        while (!stublist.empty()) {
            int e1 = stublist.back();
            stublist.pop_back();
            int e2 = stublist.back();
            stublist.pop_back();
            if (g.has_edge(e1, e2) || e1 == e2) {
                problems.emplace_back(e1);
                problems.emplace_back(e2);
            } else {
                g.add_edge(e1, e2);
            }
        }

        std::vector<std::pair<int, int>> edges = g.edges();

        while (!problems.empty()) {
            // randomly select an edge
            unsigned long idx = (*gen)() % edges.size();

            if (problems[0] != edges[idx].first && problems[1] != edges[idx].second &&
                !g.has_edge(problems[0], edges[idx].first) && !g.has_edge(problems[1], edges[idx].second)) {
                g.remove_edge(edges[idx].first, edges[idx].second);
                g.add_edge(problems[0], edges[idx].first);
                g.add_edge(problems[1], edges[idx].second);

                edges.emplace_back(std::make_pair(problems[1], edges[idx].second));
                edges[idx].second = problems[0];
                problems.erase(problems.begin(), problems.begin() + 2);
            }

        }
    }

    /*!
     * The standard configuartion model.
     *
     * If g is a multigraph then parallel edges are allowed otherwise they are simply ignored.
     *
     * @tparam T edgelist datatype
     * @param deg_sequence to use
     * @param[out] g to modify
     */
    template<typename T>
    void configuration_model(std::vector<unsigned int> &deg_sequence, BaseGraph<T> &g, std::mt19937_64 *gen = nullptr) {
        int M = std::accumulate(deg_sequence.begin(), deg_sequence.end(), 0);
        int N = deg_sequence.size();

        if (gen == nullptr) {
            auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            gen = new std::mt19937_64(seed);
        }

        if (M % 2 != 0 || M > (N * (N - 1) / 2)) {
            std::cerr << "Error: configuration_model, Invalid degree sequence.\n";
            std::_Exit(1);
        }

        empty_graph(N, g);

        std::vector<int> stublist;
        stublist.reserve(M);

        for (int i = 0; i < N; ++i) {
            for (unsigned int j = 0; j < deg_sequence[i]; ++j) {
                stublist.push_back(i);
            }
        }

        std::shuffle(stublist.begin(), stublist.end(), *gen);
        while (!stublist.empty()) {
            int e1 = stublist.back();
            stublist.pop_back();
            int e2 = stublist.back();
            stublist.pop_back();
            g.add_edge(e1, e2);
        }

        std::vector<std::pair<int, int>> edges = g.edges();
    }

    template<typename T>
    void connect_components(BaseGraph<T>& g, std::mt19937_64 *gen) {
        auto comps = std::vector<std::vector<int>>();
        auto node_map = std::map<int, unsigned int>();
        components_nodes(g, comps, node_map);
        auto old_comps = components_nodes(g);

        while (comps.size() > 1) {
            auto choices = std::vector<int>(comps.size());
            for (auto i = 0UL; i < comps.size(); ++i)
                choices[i] = i;
            int c1 = choices[(*gen)() % choices.size()];
            choices.erase(choices.begin() + c1);
            int c2 = choices[(*gen)() % choices.size()];

            int x1 = comps[c1][(*gen)() % comps[c1].size()];
            int y1 = comps[c2][(*gen)() % comps[c2].size()];

            // randomly select an edge from each of our selected nodes
            auto neigh = g.edge_list(x1);
            int x2 = neigh[(*gen)() % neigh.size()];
            neigh = g.edge_list(y1);
            int y2 = neigh[(*gen)() % neigh.size()];

            g.swap_edges(edge(x1, x2), edge(y1, y2));
            update_components_nodes(g, comps, node_map, {x1, x2, y1, y2}, {c1,c2});
        }
    }

    /*!
     * The connected version of the standard configuartion model.
     * @tparam T edgelist datatype
     * @param deg_sequence to use
     * @param[out] g
     */
    template<typename T>
    void connected_configuration_model(
            std::vector<unsigned int> &deg_sequence, BaseGraph<T> &g, std::mt19937_64 *gen = nullptr) {

        if (gen == nullptr) {
            auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            gen = new std::mt19937_64(seed);
        }

        configuration_model(deg_sequence, g);

        connect_components(g, gen);
    }

    /*!
     * See above
     * @tparam T edgelist datatype
     * @param deg_sequence to use
     * @param[out] g
     * @param gen optional random number generator
     */
    template<typename T>
    void connected_simple_configuration_model(
            std::vector<unsigned int> &deg_sequence, BaseGraph<T> &g, std::mt19937_64 *gen = nullptr) {

        simple_configuration_model(deg_sequence, g);

        if (gen == nullptr) {
            auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            gen = new std::mt19937_64(seed);
        }

        connect_components(g, gen);
    }
} /*namespace graphlib*/
